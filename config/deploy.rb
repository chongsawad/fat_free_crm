default_run_options[:pty] = true

set :repository, 'git@github.com:Chongsawad/fat_free_crm.git'
set :domain, "khunpa@10.101.102.3"

set :scm, :git
set :scm_verbose, true
set :branch, "master"
#set :git, "/usr/local/git/bin/git"
set :git, "/usr/bin/git"
set :rake, "/var/lib/gems/1.8/bin/rake"

set :user, ENV['USER'] || 'unkhown_user' 
set :application, ENV['APPNAME'] || 'unknown_appname'
set :deploy_to, "/var/www/users/#{user}/#{application}"
set :deploy_via, :checkout
set :password, "kckctv9t2925"

role :app, domain 
role :web, domain 
role :db,  domain , :primary => true

after "deploy:setup", "deploy:restart"
after "deploy:restart", "database:config"
after "database:config", "database:create"
after "database:create", "database:setup"

namespace :deploy do

  task :restart, :roles => :app do
    run "touch #{current_path}/tmp/restart.txt"
    run "mkdir -p #{shared_path}/log"
    run "mkdir -p #{shared_path}/pids"
    run "mkdir -p #{shared_path}/system"
  end
end

namespace :database do

  task :config do
    puts "------------- Generate Databaseyml ---------------"
    db_config = ERB.new <<-EOF
base: &base
  adapter: mysql
  encoding: utf8
  reconnect: false
  pool: 5
  username: root
  password:
  host: localhost

development:
  database: #{application}_development
  <<: *base

test:
  database: #{application}_test
  <<: *base

production:
  database: #{application}_production
  <<: *base
EOF
    run "mkdir -p #{current_path}/config/"
    put db_config.result, "#{current_path}/config/database.yml"
  end

  task :create do
    puts "------------- Create Database ---------------"
    rake = fetch(:rake,"rake")
    rails_env = fetch(:rails_env, "production")
    run "cd #{current_release}; #{rake} RAILS_ENV=#{rails_env} db:create"
  end

  task :drop do
    puts "------------- Drop Database ---------------"
    rake = fetch(:rake,"rake")
    rails_env = fetch(:rails_env, "production")
    run "cd #{current_release}; #{rake} RAILS_ENV=#{rails_env} db:drop"
  end

  task :remigrate do
    puts "------------- Remigrate Database ---------------"
    rake = fetch(:rake,"rake")
    rails_env = fetch(:rails_env, "production")
    run "cd #{current_release}; #{rake} RAILS_ENV=#{rails_env} db:migrate:reset"
  end
  
  after "database:remigrate","database:setup"

  task :setup do
    rake = fetch(:rake,"rake")
    rails_env = fetch(:rails_env, "production")
    puts "------------- CRM SETUP ---------------"
    run "cd #{current_release}; #{rake} RAILS_ENV=#{rails_env} crm:setup USERNAME=admin PASSWORD=admin EMAIL=admin@project.com"
  end
end

